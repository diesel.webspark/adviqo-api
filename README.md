
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3080](http://localhost:3080) to view it in the browser.

### `npm start:dev`

Runs the app in the development mode.\
Updates app on changes.\
Open [http://localhost:3080](http://localhost:3080) to view it in the browser.

### `npm run build`

Builds the app for production to the `dist` folder.

### `npm run tslint`

Runs tslint check

## Description

I made this project with TypeScript and Express. On the first launch app will create fake data and save to json file.
App communicates via REST API and gives users list on /api/users route with GET request. All sorting and filtering logic performed at controller.
