import * as express from "express";
import { Logger } from "../../../logger/logger";
import Controller from "./controller";

class User {
    public express: express.Router;
    public logger: Logger;
    public controller: Controller;

    constructor() {
        this.express = express.Router();
        this.controller = new Controller();
        this.routes();
        this.logger = new Logger();
    }

    private routes(): void {
        this.express.get('/users', this.controller.getUsers);
    }
}

export default new User().express;
