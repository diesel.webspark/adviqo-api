import {faker} from "@faker-js/faker";
import * as fs from "fs";
import CONSTANTS from "../../constants";
import {IUser} from "../../interfaces/IUser";
import {IUserRequestQuery} from "../../interfaces/IUserRequestQuery";
import {Languages} from "../../enums/Languages";
import {randomInteger} from "../../utils";
import {Statuses} from "../../enums/Statuses";
import {Sorting} from "../../enums/Sorting";

const {DATA_FILE_NAME, ENTRIES_PER_PAGE} = CONSTANTS;

class UsersService {
    public users: IUser[] = [];

    constructor() {
        this.createFakeData();
    }

    private createFakeData(): void {
        fs.access(DATA_FILE_NAME, fs.constants.F_OK, (err) => {
            if (err) {
                this.users = this.generateUsers();
                fs.writeFile(DATA_FILE_NAME, JSON.stringify(this.generateUsers()), () => {
                });
            } else {
                UsersService.getDataAsync().then(data => {
                    try {
                        this.users = JSON.parse(data);
                    } catch (e) {
                        this.users = [];
                    }
                })
            }
        })
    }

    static getDataAsync(): Promise<string> {
        return fs.promises.readFile(DATA_FILE_NAME, {encoding: 'utf8'});
    }

    generateUsers(): IUser[] {
        const users = [];

        for (let id = 1; id <= 100; id++) {
            const firstName = faker.name.firstName();
            const lastName = faker.name.lastName();
            const languages = faker.helpers.arrayElements(Object.values(Languages));
            const reviews = randomInteger(5, 100);
            const status = faker.helpers.arrayElement(Object.values(Statuses));
            const avatar = faker.image.cats(200, 200, true);

            users.push({
                id,
                firstName,
                lastName,
                languages,
                reviews,
                status,
                avatar
            });
        }

        return users;
    }

    public getUsers(query: IUserRequestQuery): { isLastPage: boolean, list: IUser[] } {
        const {
            languages,
            status,
            sorting,
            page = 1,
            entriesPerPage = ENTRIES_PER_PAGE
        } = query;

        let mainData = this.users;

        if (sorting) {
            mainData.sort((prev, next) => {
                if (sorting === Sorting.ASCENDING) {
                    return prev.reviews - next.reviews;
                }
                return next.reviews - prev.reviews;
            })
        }

        if (languages) {
            mainData = mainData.filter((entry) => {
                return languages.some((language: Languages) => entry.languages.includes(language));
            })
        }

        if (status) {
            mainData = mainData.filter((entry) => entry.status === status)
        }

        const lastIndex = entriesPerPage * page;
        const firstIndex = entriesPerPage * (page - 1);
        const list = mainData.slice(firstIndex, lastIndex);
        const isLastPage = list.length < entriesPerPage;
        return {
            isLastPage,
            list
        }
    }
}

export default new UsersService();

