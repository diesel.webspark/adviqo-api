import { Logger } from "../../../logger/logger";
import {IUserRequestQuery} from "../../interfaces/IUserRequestQuery";
import UsersData from "./userService";
import {NextFunction, Request, Response} from "express";
import {randomInteger} from "../../utils";

class Controller {
    public logger: Logger;

    constructor() {
        this.logger = new Logger();
    }

    public getUsers = (req: Request<any, any, any, IUserRequestQuery>, res:Response<any>, next:NextFunction):void => {
       this.logger.info('url:' + req.url);
        try {
            const data = UsersData.getUsers(req.query);
            const delay = randomInteger(0, 2) * 1000;
            setTimeout(()=> {
                res.json(data)
            }, delay)

        } catch (e) {
            this.logger.debug(e)
            res.status(500).send('Sorry cant handle that!');
        }
    }
}

export default Controller;
