export enum Sorting {
    ASCENDING = 'ascending',
    DESCENDING = 'descending',
}
