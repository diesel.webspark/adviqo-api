import {Statuses} from "../enums/Statuses";
import {Sorting} from "../enums/Sorting";
import {Languages} from "../enums/Languages";

export interface IUserRequestQuery {
    languages?: Languages[];
    status?: Statuses;
    sorting?: Sorting;
    page?: number;
    entriesPerPage?: number;
}
