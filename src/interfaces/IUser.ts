import {Languages} from "../enums/Languages";
import {Statuses} from "../enums/Statuses";


export interface IUser {
    firstName: string;
    lastName: string;
    id: number;
    reviews: number;
    languages: Languages[];
    avatar: string;
    status: Statuses;
}
