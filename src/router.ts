import * as express from "express";
import { Logger } from "../logger/logger";
import User from "./components/users";

class Router {
    public express: express.Router;
    public logger: Logger;

    constructor() {
        this.express = express.Router();
        this.routes();
        this.logger = new Logger();
    }

    private routes(): void {
        this.express.use("/", User);
    }
}

export default new Router().express;
