import * as express from "express";
import { Logger } from "../logger/logger";
import Router from "./router";
import * as cors from "cors";

class App {
    public express: express.Application;
    public logger: Logger;

    constructor() {
        this.express = express();
        this.cors();
        this.routes();
        this.logger = new Logger();
    }

    private cors():void {
        this.express.use(cors());
    }

    private routes(): void {
        this.express.use("/api", Router);

        this.express.use("*", (req, res, next) => {
            res.send("Make sure url is correct!!!");
        });
    }
}

export default new App().express;
